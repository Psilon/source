# Ссылки, редакторы, плагины, утилиты

## Репозитории
* [Github](https://github.com/) (удобное бесплатное хранилище для публичных проектов, хранения кусков кода)
* [Bitbucket](https://bitbucket.org) (удобное бесплатное хранилище для закрытых проектов с командой до 5 человек)

## Ссылки
* [Node](https://nodejs.org/en/) официальный сайт проекта Node.js
* [npm](https://www.npmjs.com/) хранилище для npm пакетов

## Редакторы
* [Atom](https://atom.io/) (бесплатный открытый проект от команды Github)
* [Sublime](https://www.sublimetext.com/3) (условно бесплатный, каждые 2 часа всплывает окно с предложением купить)
* [Brackets](http://brackets.io/) (бесплатный)
* [WebStorm](https://www.jetbrains.com/webstorm/) (Trial 30 дней)
* [PhpStorm](https://www.jetbrains.com/phpstorm/) (Trial 30 дней)

## Полезные ресурсы
* [Frontend WhiteList](https://github.com/melnik909/frontend-whitelist) — это список полезных и интересных авторских статей, переводов, докладов и ресурсов на русском языке для разработчиков интерфейсов.
* [Channels](https://github.com/andrew--r/channels#russian-language) — коллеция видео каналов нв Youtube про Web разработку, JavaScript, CSS, HTML и сопутствующие темы. 