# Atom

## Плагины
* [emmet](https://atom.io/packages/emmet) - предназначен для быстрой правки _html_ и _css_ файлов
* [open-terminal-here](https://atom.io/packages/open-terminal-here) - открывает терминал (консоль) в рабочей директории проекта
* [platformio-ide-terminal](https://atom.io/packages/platformio-ide-terminal) - открывает терминал в редакторе. Горячее сочетание клавиш по умолчанию _ctrl+`_
* [atom-pair](https://atom.io/packages/atom-pair) - парное программирование
* [project-manager](https://atom.io/packages/project-manager) - менеджер управления проектами
* [git-plus](https://atom.io/packages/git-plus) - выполняет команды git без терминала